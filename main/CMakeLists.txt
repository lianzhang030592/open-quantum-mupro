cmake_minimum_required(VERSION 3.9...3.15)
if(${CMAKE_VERSION} VERSION_LESS 3.12)
  cmake_policy(VERSION ${CMAKE_MAJOR_VERSION}.${CMAKE_MINOR_VERSION})
else()
endif()

message(STATUS "CMake version is " ${CMAKE_MAJOR_VERSION}.${CMAKE_MINOR_VERSION})
set(exe_name qm)
project(
  ${exe_name}
  VERSION 1.0
  DESCRIPTION "The quantum material phase field simulation based on PETSC"
  LANGUAGES C
)

set(CMAKE_VERBOSE_MAKEFILE TRUE CACHE BOOL "Show compiling commands")
set(CMAKE_INSTALL_PREFIX "$ENV{HOME}/qm" CACHE STRING "The install prefix of quantum material module")
set(CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake")
message(STATUS ${CMAKE_MODULE_PATH})
#set(PETSC_DIR "/opt/aci/sw/petsc/3.8.3_intel-16.0.3_impi-5.1.3")

if(NOT CMAKE_BUILD_TYPE AND NOT CMAKE_CONFIGURATION_TYPES)
  set(CMAKE_BUILD_TYPE "Release" CACHE STRING "Choose the type of build." FORCE)
  set_property(CACHE CMAKE_BUILD_TYPE PROPERTY STRINGS "Debug" "Release" "MinSizeRel" "RelWithDebInfo")
endif()

add_executable(
  ${exe_name}
  poisson2D.c
)
find_package(PETSc)

target_link_libraries(
  ${exe_name}
  PUBLIC
  ${PETSC_LIBRARIES}
  )

target_include_directories(
  ${exe_name}
  PRIVATE
  ${PETSC_INCLUDES}
  )
