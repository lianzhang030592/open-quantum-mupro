# Open source Quantum Material Phase-field Package
## To build the project
Go into the build directory to use cmake commands
```
cd build
cmake ..
```
## Version control
### Use git for version control, please learn how to use git first.
[Pro git book, have everything, good for a starter to obtain an overview](https://git-scm.com/book/en/v2) 
[A more concise and visual way of introducing git commands](https://marklodato.github.io/visual-git-guide/index-en.html)
[Learn about branching interactively online](https://learngitbranching.js.org)
[Git cheat sheet](https://github.github.com/training-kit/downloads/github-git-cheat-sheet/)

### Committing rules:
1. Keep each commit small and well defined. For example, limit each commit to only one function, class or one bug. As little as one line of code change could be one commit.
2. Write meaningful message for each commit 

### Branching rules:
1. Keep master clean. Master is reserved for stable tested release. Do not push directly to master!
2. rc branch is for release candidate. Submit pull request to merge your own branch into the rc branch. After everything is tested on the rc branch, it will be merged into the master branch for next release.
3. All other branches should be feature oriented, that each one should be for the purpose of implementing one new feature. If you want to work on two features at the same time, please create two branches.
3. Name your branch following the convention, [user id]-[branch goal], for example, xuc116-inputParser

### Versioning rules:
1. X.Y.Z correspond to major.minor.patch

## Build system
### Use cmake to compile the whole project, please learn modern cmake first.
[Great article to get to know modern cmake](https://cliutils.gitlab.io/modern-cmake/). 
Follow the modern cmake style, instead of the old cmake style. For example, treat everything from a target point of view.

## Unit testing
### Use google test for unit testing, please learn gtest first.
[Great article of using google test with cmake](https://cheukyin699.github.io/tutorial/2017/12/19/googletest-tutorial.html)
[Testing class](http://www.yolinux.com/TUTORIALS/Cpp-GoogleTest.html)
Each class should have a corresponding \*Test class, and each individual function should have a test function.

## Coding style
[Good practice and guidelines](https://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines)
- use .c for c file, .cpp for c++ file and .h for header file
- all file name must be lower case, without dash or underscore. For example, myfirstclass.cpp

